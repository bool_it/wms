/*
Navicat MySQL Data Transfer

Source Server         : seho
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : wms

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-07-12 10:09:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_admin
-- ----------------------------
DROP TABLE IF EXISTS `b_admin`;
CREATE TABLE `b_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(16) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `head_img` varchar(255) DEFAULT NULL COMMENT '头像',
  `nickname` varchar(40) DEFAULT NULL COMMENT '用户昵称',
  `phone` varchar(12) DEFAULT NULL COMMENT '手机',
  `email` varchar(40) DEFAULT NULL COMMENT '邮箱',
  `role_id` varchar(255) DEFAULT NULL COMMENT '角色id',
  `last_time` int(11) unsigned DEFAULT NULL COMMENT '最后登陆时间',
  `last_ip` varchar(40) DEFAULT NULL COMMENT '最后登陆ip',
  `create_time` int(11) unsigned NOT NULL DEFAULT '1320981071' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of b_admin
-- ----------------------------
INSERT INTO `b_admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'https://avatars1.githubusercontent.com/u/37901036?v=4', '管理员', '17052850083', '81001985@qq.com', '2', '1562897329', '127.0.0.1', '1320981071');
INSERT INTO `b_admin` VALUES ('8', 'bool', 'c506ff134babdd6e68ab3e6350e95305', null, '布尔', '', '', '5', '1553048096', '127.0.0.1', '1552986182');

-- ----------------------------
-- Table structure for b_admin_node
-- ----------------------------
DROP TABLE IF EXISTS `b_admin_node`;
CREATE TABLE `b_admin_node` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级id',
  `ico` varchar(20) DEFAULT NULL COMMENT 'ico图标',
  `name` varchar(20) NOT NULL COMMENT '节点名称',
  `controller` varchar(20) NOT NULL COMMENT '控制器',
  `action` varchar(20) NOT NULL DEFAULT '' COMMENT '方法',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `desc` varchar(200) NOT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='节点表';

-- ----------------------------
-- Records of b_admin_node
-- ----------------------------
INSERT INTO `b_admin_node` VALUES ('10', '4', '', '添加', 'Role', 'create', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('2', '0', 'fa-cogs', '权限管理', '', '', '0', '', '1540533340');
INSERT INTO `b_admin_node` VALUES ('3', '2', '', '用户管理', 'admin', 'index', '0', '', '1540533340');
INSERT INTO `b_admin_node` VALUES ('4', '2', '', '角色管理', 'role', 'index', '0', '', '1540533340');
INSERT INTO `b_admin_node` VALUES ('6', '3', '', '添加', 'admin', 'create', '0', '', '1540533340');
INSERT INTO `b_admin_node` VALUES ('7', '3', '', '编辑', 'admin', 'edit', '0', '', '1540533340');
INSERT INTO `b_admin_node` VALUES ('11', '4', '', '编辑', 'Role', 'edit', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('13', '2', '', '节点管理', 'node', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('14', '0', 'fa-cogs', '基础资料', '', '', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('15', '14', '', '产品管理', 'product', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('16', '14', '', '产品分类', 'cate', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('17', '14', '', '品牌管理', 'brand', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('18', '14', '', '客户管理', 'customer', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('19', '14', '', '厂家管理', 'supplier', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('20', '14', '', '计量单位', 'unit', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('21', '14', '', '颜色管理', 'color', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('22', '14', '', '库位管理', 'location', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('23', '14', '', '物流快递', 'express', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('24', '0', 'fa-cogs', '订单管理', '', '', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('25', '24', '', '采购订单', 'purchase', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('26', '24', '', '销售订单', 'sale', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('27', '24', '', '退货订单', '', 'index', '1', '', '0');
INSERT INTO `b_admin_node` VALUES ('28', '0', 'fa-plus', '仓库管理', '', '', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('29', '28', '', '采购入库', 'indepot', 'index', '0', '', '0');
INSERT INTO `b_admin_node` VALUES ('30', '28', '', '销售出库', 'outdepot', 'index', '0', '', '0');

-- ----------------------------
-- Table structure for b_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `b_admin_role`;
CREATE TABLE `b_admin_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级角色',
  `name` varchar(60) NOT NULL COMMENT '角色名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0正常 1禁用',
  `ids` varchar(255) DEFAULT NULL COMMENT '节点id',
  `desc` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of b_admin_role
-- ----------------------------
INSERT INTO `b_admin_role` VALUES ('5', '0', '权限管理', '0', '3,4,2,6,7,11,13', '', '1552986011');
INSERT INTO `b_admin_role` VALUES ('4', '0', '仓库管理', '0', null, '', '1552984317');

-- ----------------------------
-- Table structure for b_brand
-- ----------------------------
DROP TABLE IF EXISTS `b_brand`;
CREATE TABLE `b_brand` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '名称',
  `tel` varchar(15) DEFAULT NULL COMMENT '电话',
  `url` varchar(200) DEFAULT NULL COMMENT '网址',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='快递物流';

-- ----------------------------
-- Records of b_brand
-- ----------------------------
INSERT INTO `b_brand` VALUES ('3', '华为', '0512', 'www.huawei.com', '0', '1553051237');
INSERT INTO `b_brand` VALUES ('4', '小米', '', '', '0', '1553583684');
INSERT INTO `b_brand` VALUES ('5', 'vivo', '', '', '0', '1553583688');
INSERT INTO `b_brand` VALUES ('6', 'oppo', '', '', '0', '1553583696');

-- ----------------------------
-- Table structure for b_cate
-- ----------------------------
DROP TABLE IF EXISTS `b_cate`;
CREATE TABLE `b_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned DEFAULT NULL COMMENT '父级id',
  `name` varchar(20) NOT NULL COMMENT '分类名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `desc` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='分类';

-- ----------------------------
-- Records of b_cate
-- ----------------------------
INSERT INTO `b_cate` VALUES ('4', '0', '数码', '0', '', '1553050631');

-- ----------------------------
-- Table structure for b_color
-- ----------------------------
DROP TABLE IF EXISTS `b_color`;
CREATE TABLE `b_color` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '单位名称',
  `code` varchar(20) DEFAULT NULL COMMENT '颜色码',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='颜色';

-- ----------------------------
-- Records of b_color
-- ----------------------------
INSERT INTO `b_color` VALUES ('2', '黑色', '#000000', '0', '1553059471');

-- ----------------------------
-- Table structure for b_customer
-- ----------------------------
DROP TABLE IF EXISTS `b_customer`;
CREATE TABLE `b_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) unsigned DEFAULT NULL COMMENT '客户类型，0 个人，1商家',
  `title` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `contact` varchar(20) DEFAULT NULL COMMENT '联系人',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话',
  `fax` varchar(20) DEFAULT NULL COMMENT '传真',
  `phone` varchar(12) NOT NULL COMMENT '手机号码',
  `email` varchar(40) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL COMMENT '省份',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  `district` varchar(20) DEFAULT NULL COMMENT '县区',
  `street` varchar(200) DEFAULT NULL COMMENT '街道',
  `credit_id` varchar(50) DEFAULT NULL COMMENT '统一社会信用代码',
  `taxpayer_id` varchar(255) DEFAULT NULL COMMENT '纳税人识别号',
  `desc` varchar(200) DEFAULT NULL COMMENT '备注',
  `bank` varchar(20) DEFAULT NULL COMMENT '开户行',
  `bank_number` varchar(30) DEFAULT NULL COMMENT '开户银行卡号',
  `bank_address` varchar(200) DEFAULT NULL COMMENT '开户行地址',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='客户管理';

-- ----------------------------
-- Records of b_customer
-- ----------------------------
INSERT INTO `b_customer` VALUES ('4', '1', '苏州智慧龙网络可以有限公司', '苏州智慧龙', '', '', '18506292124', '30024167@qq.com', '江苏省', '苏州市', '吴中区', '东平街282号', '', '', '', '中国工商银行', '', '', '0', '1553059224');
INSERT INTO `b_customer` VALUES ('5', '0', '', '山东', '', '', '185062921245', '185062921245', '山东省', '济南市', '历下区', '111', '', '', '', '中国工商银行', '', '', '0', '1553584703');
INSERT INTO `b_customer` VALUES ('6', '1', '内蒙古', '11', '', '', '11', '11', '内蒙古自治区', '呼和浩特市', '新城区', '11', '', '', '', '中国工商银行', '', '', '0', '1553584753');

-- ----------------------------
-- Table structure for b_express
-- ----------------------------
DROP TABLE IF EXISTS `b_express`;
CREATE TABLE `b_express` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '名称',
  `tel` varchar(15) DEFAULT NULL COMMENT '电话',
  `url` varchar(200) DEFAULT NULL COMMENT '网址',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='快递物流';

-- ----------------------------
-- Records of b_express
-- ----------------------------
INSERT INTO `b_express` VALUES ('2', '顺丰快递', '05612', '', '0', '1553135455');

-- ----------------------------
-- Table structure for b_indepot
-- ----------------------------
DROP TABLE IF EXISTS `b_indepot`;
CREATE TABLE `b_indepot` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(100) NOT NULL COMMENT '入库单号',
  `type` varchar(40) DEFAULT NULL COMMENT '入库类型',
  `purchase` int(11) unsigned DEFAULT NULL COMMENT '采购订单号',
  `author` varchar(40) DEFAULT NULL COMMENT '操作人',
  `supplier` int(11) unsigned DEFAULT NULL COMMENT '供应商',
  `in_date` varchar(20) DEFAULT NULL COMMENT '入库日期',
  `in_time` int(11) unsigned DEFAULT NULL COMMENT '入库时间',
  `count` decimal(10,2) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='入库操作';

-- ----------------------------
-- Records of b_indepot
-- ----------------------------
INSERT INTO `b_indepot` VALUES ('3', '20190321023131855', '1', '1', '管理员', '3', '2019-03-21', '1553097600', '2999.00', '0', '1553150080');
INSERT INTO `b_indepot` VALUES ('23', '20190321031421639', '2', '2', '管理员', '3', '2019-03-21', '1553097600', '29990.00', '0', '1553152553');
INSERT INTO `b_indepot` VALUES ('24', '20190712100723667', '1', '5', '管理员', '3', '2019-07-12', '1562860800', '54985.00', '0', '1562897248');
INSERT INTO `b_indepot` VALUES ('25', '20190712100730297', '1', '6', '管理员', '3', '2019-07-12', '1562860800', '2999.00', '0', '1562897253');

-- ----------------------------
-- Table structure for b_indepot_main
-- ----------------------------
DROP TABLE IF EXISTS `b_indepot_main`;
CREATE TABLE `b_indepot_main` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fid` int(11) unsigned NOT NULL COMMENT '父级的id',
  `pid` int(11) unsigned NOT NULL COMMENT '产品id',
  `sid` int(11) unsigned NOT NULL COMMENT '采购订单的id',
  `num` int(11) unsigned NOT NULL COMMENT '数量',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '价格',
  `brand` int(11) unsigned DEFAULT NULL COMMENT '品牌id',
  `color` int(11) unsigned DEFAULT NULL COMMENT '颜色id',
  `unit` int(11) unsigned DEFAULT NULL COMMENT '单位id',
  `depot` int(11) unsigned DEFAULT NULL,
  `location` int(11) unsigned DEFAULT NULL COMMENT '库位',
  `count` decimal(10,2) unsigned DEFAULT NULL COMMENT '合计',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='采购明细';

-- ----------------------------
-- Records of b_indepot_main
-- ----------------------------
INSERT INTO `b_indepot_main` VALUES ('3', '3', '6', '1', '1', '2999.00', '3', '2', '4', '0', '3', '2999.00', '1553150080');
INSERT INTO `b_indepot_main` VALUES ('11', '23', '6', '3', '10', '2999.00', '3', '2', '4', '0', '3', '29990.00', '1553152553');
INSERT INTO `b_indepot_main` VALUES ('12', '24', '8', '4', '10', '3999.00', '6', '2', '4', '0', '3', '39990.00', '1562897248');
INSERT INTO `b_indepot_main` VALUES ('13', '24', '7', '5', '5', '2999.00', '4', '2', '4', '0', '3', '14995.00', '1562897248');
INSERT INTO `b_indepot_main` VALUES ('14', '25', '7', '6', '1', '2999.00', '4', '2', '4', '0', '3', '2999.00', '1562897253');

-- ----------------------------
-- Table structure for b_location
-- ----------------------------
DROP TABLE IF EXISTS `b_location`;
CREATE TABLE `b_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) unsigned DEFAULT NULL COMMENT '所属仓库',
  `name` varchar(20) DEFAULT NULL COMMENT '库位',
  `status` tinyint(1) unsigned NOT NULL COMMENT '状态: 0正常 1禁用',
  `shelve` varchar(255) NOT NULL DEFAULT '默认' COMMENT '货架',
  `desc` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='库位管理';

-- ----------------------------
-- Records of b_location
-- ----------------------------
INSERT INTO `b_location` VALUES ('3', null, '数码家电', '0', 'A,B,C,D', '', '1553059984');

-- ----------------------------
-- Table structure for b_outdepot
-- ----------------------------
DROP TABLE IF EXISTS `b_outdepot`;
CREATE TABLE `b_outdepot` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(100) NOT NULL COMMENT '入库单号',
  `type` varchar(40) DEFAULT NULL COMMENT '入库类型',
  `purchase` int(11) unsigned DEFAULT NULL COMMENT '采购订单号',
  `author` varchar(40) DEFAULT NULL COMMENT '操作人',
  `customer` int(11) unsigned DEFAULT NULL COMMENT '客户',
  `in_date` varchar(20) DEFAULT NULL COMMENT '入库日期',
  `in_time` int(11) unsigned DEFAULT NULL COMMENT '入库时间',
  `count` decimal(10,2) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='入库操作';

-- ----------------------------
-- Records of b_outdepot
-- ----------------------------
INSERT INTO `b_outdepot` VALUES ('1', '20190321023642687', '1', '1', '管理员', '4', '2019-03-21', '1553097600', '2999.00', '0', '1553150211');
INSERT INTO `b_outdepot` VALUES ('3', '20190321032718382', '2', '0', '管理员', '4', '2019-03-21', '1553097600', '299900.00', '0', '1553153318');
INSERT INTO `b_outdepot` VALUES ('4', '20190712100737359', '1', '3', '管理员', '4', '2019-07-12', '1562860800', '5998.00', '0', '1562897260');
INSERT INTO `b_outdepot` VALUES ('5', '20190712100800158', '1', '5', '管理员', '4', '2019-07-12', '1562860800', '2999.00', '0', '1562897286');

-- ----------------------------
-- Table structure for b_outdepot_main
-- ----------------------------
DROP TABLE IF EXISTS `b_outdepot_main`;
CREATE TABLE `b_outdepot_main` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fid` int(11) unsigned NOT NULL COMMENT '父级的id',
  `pid` int(11) unsigned NOT NULL COMMENT '产品id',
  `sid` int(11) unsigned NOT NULL COMMENT '采购订单的id',
  `num` int(11) unsigned NOT NULL COMMENT '数量',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '价格',
  `brand` int(11) unsigned DEFAULT NULL COMMENT '品牌id',
  `color` int(11) unsigned DEFAULT NULL COMMENT '颜色id',
  `unit` int(11) unsigned DEFAULT NULL COMMENT '单位id',
  `depot` int(11) unsigned DEFAULT NULL,
  `location` int(11) unsigned DEFAULT NULL COMMENT '库位',
  `count` decimal(10,2) unsigned DEFAULT NULL COMMENT '合计',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='采购明细';

-- ----------------------------
-- Records of b_outdepot_main
-- ----------------------------
INSERT INTO `b_outdepot_main` VALUES ('1', '1', '6', '1', '1', '2999.00', '3', '2', '4', '0', '3', '2999.00', '1553150211');
INSERT INTO `b_outdepot_main` VALUES ('2', '3', '6', '6', '100', '2999.00', '3', '2', '4', '0', '3', '299900.00', '1553153318');
INSERT INTO `b_outdepot_main` VALUES ('3', '4', '7', '3', '2', '2999.00', '4', '2', '4', '0', '3', '5998.00', '1562897260');
INSERT INTO `b_outdepot_main` VALUES ('4', '5', '7', '5', '1', '2999.00', '4', '2', '4', '0', '3', '2999.00', '1562897286');

-- ----------------------------
-- Table structure for b_plug
-- ----------------------------
DROP TABLE IF EXISTS `b_plug`;
CREATE TABLE `b_plug` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL COMMENT '名称',
  `code` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL COMMENT '状态',
  `path` varchar(255) NOT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of b_plug
-- ----------------------------

-- ----------------------------
-- Table structure for b_product
-- ----------------------------
DROP TABLE IF EXISTS `b_product`;
CREATE TABLE `b_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(100) NOT NULL COMMENT '商品编号',
  `unique_sn` varchar(100) DEFAULT NULL COMMENT '内部编号',
  `name` varchar(10) NOT NULL COMMENT '商品名称',
  `cate` int(11) unsigned DEFAULT NULL COMMENT '分类id',
  `brand` int(11) unsigned DEFAULT NULL COMMENT '品牌id',
  `model` varchar(50) DEFAULT NULL COMMENT '型号',
  `spec` varchar(100) NOT NULL DEFAULT '' COMMENT '规格',
  `color` int(11) unsigned DEFAULT NULL COMMENT '颜色id',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '默认价格',
  `unit` int(11) unsigned DEFAULT NULL COMMENT '单位id',
  `supplier` int(11) unsigned DEFAULT NULL COMMENT '默认供应商',
  `customer` int(11) unsigned DEFAULT NULL COMMENT '默认客户',
  `location` int(11) unsigned DEFAULT NULL COMMENT '默认库位',
  `depot` int(11) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='产品表';

-- ----------------------------
-- Records of b_product
-- ----------------------------
INSERT INTO `b_product` VALUES ('6', '20190320012448208', '111111', '华为V20', '0', '3', 'V20', '6G', '2', '2999.00', '4', '3', '4', '3', null, '', '1553059515');
INSERT INTO `b_product` VALUES ('7', '20190326030055947', '', '小米9', '0', '4', '小米9', '6G', '2', '2999.00', '4', '3', '4', '3', null, null, '1553583674');
INSERT INTO `b_product` VALUES ('8', '20190326030154221', '', 'oppo x20', '0', '6', ' x20', '6G', '2', '3999.00', '4', '3', '4', '3', null, null, '1553583738');

-- ----------------------------
-- Table structure for b_purchase
-- ----------------------------
DROP TABLE IF EXISTS `b_purchase`;
CREATE TABLE `b_purchase` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(100) NOT NULL COMMENT '编号',
  `channel` varchar(100) DEFAULT NULL COMMENT '销售渠道',
  `author` varchar(40) DEFAULT NULL COMMENT '销售人',
  `purchase_date` varchar(20) DEFAULT NULL COMMENT '销售日期',
  `purchase_time` int(11) unsigned NOT NULL COMMENT '销售时间',
  `supplier` int(11) unsigned DEFAULT NULL COMMENT '供应商',
  `count` decimal(10,2) unsigned DEFAULT NULL COMMENT '总价',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否入库',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='采购';

-- ----------------------------
-- Records of b_purchase
-- ----------------------------
INSERT INTO `b_purchase` VALUES ('1', '20190321020322129', '天猫', '管理员', '2019-03-21', '1553097600', '3', '299900.00', '1', '0', '1553148216');
INSERT INTO `b_purchase` VALUES ('2', '20190321031406818', '天猫', '管理员', '2019-02-21', '1553097600', '3', '2999.00', '1', '0', '1553152456');
INSERT INTO `b_purchase` VALUES ('5', '20190326030706303', '京东', '管理员', '2019-03-26', '1553529600', '3', '54985.00', '1', '0', '1553584187');
INSERT INTO `b_purchase` VALUES ('6', '20190326031349402', '京东', '管理员', '2019-03-26', '1553529600', '3', '2999.00', '1', '0', '1553584442');
INSERT INTO `b_purchase` VALUES ('7', '20190326031551707', '天猫', '管理员', '2019-03-26', '1553529600', '3', '5998.00', '0', '0', '1553584566');
INSERT INTO `b_purchase` VALUES ('8', '20190712100652151', '天猫', '管理员', '2019-07-12', '1562860800', '3', '59980.00', '0', '0', '1562897234');

-- ----------------------------
-- Table structure for b_purchase_main
-- ----------------------------
DROP TABLE IF EXISTS `b_purchase_main`;
CREATE TABLE `b_purchase_main` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL COMMENT '产品id',
  `sid` int(11) unsigned NOT NULL COMMENT '采购订单的id',
  `num` int(11) unsigned NOT NULL COMMENT '数量',
  `snum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '入库数量',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '价格',
  `brand` int(11) unsigned DEFAULT NULL COMMENT '品牌id',
  `color` int(11) unsigned DEFAULT NULL COMMENT '颜色id',
  `unit` int(11) unsigned DEFAULT NULL COMMENT '单位id',
  `depot` int(11) unsigned DEFAULT NULL,
  `location` int(11) unsigned DEFAULT NULL COMMENT '库位',
  `count` decimal(10,2) unsigned DEFAULT NULL COMMENT '合计',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='采购明细';

-- ----------------------------
-- Records of b_purchase_main
-- ----------------------------
INSERT INTO `b_purchase_main` VALUES ('1', '6', '1', '100', '100', '2999.00', '3', '2', '4', '0', '3', '299900.00', '1553148216');
INSERT INTO `b_purchase_main` VALUES ('3', '6', '2', '1', '1', '2999.00', '3', '2', '4', '0', '3', '2999.00', '1553152456');
INSERT INTO `b_purchase_main` VALUES ('4', '8', '5', '10', '10', '3999.00', '6', '2', '4', null, '3', '39990.00', '1553584187');
INSERT INTO `b_purchase_main` VALUES ('5', '7', '5', '5', '5', '2999.00', '4', '2', '4', null, '3', '14995.00', '1553584187');
INSERT INTO `b_purchase_main` VALUES ('6', '7', '6', '1', '1', '2999.00', '4', '2', '4', null, '3', '2999.00', '1553584442');
INSERT INTO `b_purchase_main` VALUES ('7', '7', '7', '2', '2', '2999.00', '4', '2', '4', null, '3', '5998.00', '1553584566');
INSERT INTO `b_purchase_main` VALUES ('8', '6', '8', '20', '20', '2999.00', '3', '2', '4', null, '3', '59980.00', '1562897234');

-- ----------------------------
-- Table structure for b_sale
-- ----------------------------
DROP TABLE IF EXISTS `b_sale`;
CREATE TABLE `b_sale` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sn` varchar(100) NOT NULL COMMENT '编号',
  `channel` varchar(100) DEFAULT NULL COMMENT '销售渠道',
  `author` varchar(40) DEFAULT NULL COMMENT '销售人',
  `sale_date` varchar(20) DEFAULT NULL COMMENT '销售日期',
  `sale_time` int(11) unsigned NOT NULL COMMENT '销售时间',
  `customer` int(11) unsigned DEFAULT NULL COMMENT '客户id',
  `count` decimal(10,2) unsigned DEFAULT NULL COMMENT '总价',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否出库',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='销售';

-- ----------------------------
-- Records of b_sale
-- ----------------------------
INSERT INTO `b_sale` VALUES ('1', '20190321021909157', '天猫', '管理员', '2019-03-21', '1553097600', '4', '2999.00', '1', '0', '1553149157');
INSERT INTO `b_sale` VALUES ('2', '20190326024836402', '京东', '管理员', '2019-03-26', '1553529600', '4', '2999.00', '0', '0', '1553582928');
INSERT INTO `b_sale` VALUES ('3', '20190326031939241', '淘宝', '管理员', '2019-03-26', '1553529600', '4', '5998.00', '1', '0', '1553584791');
INSERT INTO `b_sale` VALUES ('4', '20190326032002647', '门店', '管理员', '2019-03-26', '1553529600', '4', '3999.00', '0', '0', '1553584814');
INSERT INTO `b_sale` VALUES ('5', '20190712100745803', '天猫', '管理员', '2019-07-12', '1562860800', '4', '2999.00', '1', '0', '1562897275');

-- ----------------------------
-- Table structure for b_sale_main
-- ----------------------------
DROP TABLE IF EXISTS `b_sale_main`;
CREATE TABLE `b_sale_main` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL COMMENT '产品id',
  `sid` int(11) unsigned NOT NULL COMMENT '销售订单的id',
  `num` int(11) unsigned NOT NULL COMMENT '数量',
  `snum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出库数量',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '价格',
  `brand` int(11) unsigned DEFAULT NULL COMMENT '品牌id',
  `color` int(11) unsigned DEFAULT NULL COMMENT '颜色id',
  `unit` int(11) unsigned DEFAULT NULL COMMENT '单位id',
  `depot` int(11) unsigned DEFAULT NULL,
  `location` int(11) unsigned DEFAULT NULL COMMENT '库位',
  `count` decimal(10,2) unsigned DEFAULT NULL COMMENT '合计',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='销售明细';

-- ----------------------------
-- Records of b_sale_main
-- ----------------------------
INSERT INTO `b_sale_main` VALUES ('1', '6', '1', '1', '1', '2999.00', '3', '2', '4', '0', '3', '2999.00', '1553149157');
INSERT INTO `b_sale_main` VALUES ('2', '6', '2', '1', '1', '2999.00', '3', '2', '4', '0', '3', '2999.00', '1553582928');
INSERT INTO `b_sale_main` VALUES ('3', '7', '3', '2', '2', '2999.00', '4', '2', '4', '0', '3', '5998.00', '1553584791');
INSERT INTO `b_sale_main` VALUES ('4', '8', '4', '1', '1', '3999.00', '6', '2', '4', '0', '3', '3999.00', '1553584814');
INSERT INTO `b_sale_main` VALUES ('5', '7', '5', '1', '1', '2999.00', '4', '2', '4', '0', '3', '2999.00', '1562897275');

-- ----------------------------
-- Table structure for b_stock
-- ----------------------------
DROP TABLE IF EXISTS `b_stock`;
CREATE TABLE `b_stock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL COMMENT '产品id',
  `num` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='库存表';

-- ----------------------------
-- Records of b_stock
-- ----------------------------
INSERT INTO `b_stock` VALUES ('2', '6', '0', '1553059515', '1553059515');
INSERT INTO `b_stock` VALUES ('3', '7', '3', '1553583674', '1553583674');
INSERT INTO `b_stock` VALUES ('4', '8', '10', '1553583738', '1553583738');

-- ----------------------------
-- Table structure for b_supplier
-- ----------------------------
DROP TABLE IF EXISTS `b_supplier`;
CREATE TABLE `b_supplier` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `contact` varchar(20) DEFAULT NULL COMMENT '联系人',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话',
  `fax` varchar(20) DEFAULT NULL COMMENT '传真',
  `phone` varchar(12) NOT NULL COMMENT '手机号码',
  `email` varchar(40) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL COMMENT '省份',
  `city` varchar(20) DEFAULT NULL COMMENT '城市',
  `district` varchar(20) DEFAULT NULL COMMENT '县区',
  `street` varchar(200) DEFAULT NULL COMMENT '街道',
  `credit_id` varchar(50) DEFAULT NULL COMMENT '统一社会信用代码',
  `taxpayer_id` varchar(255) DEFAULT NULL COMMENT '纳税人识别号',
  `desc` varchar(200) DEFAULT NULL COMMENT '备注',
  `bank` varchar(20) DEFAULT NULL COMMENT '开户行',
  `bank_number` varchar(30) DEFAULT NULL COMMENT '开户银行卡号',
  `bank_address` varchar(200) DEFAULT NULL COMMENT '开户行地址',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='供应商管理';

-- ----------------------------
-- Records of b_supplier
-- ----------------------------
INSERT INTO `b_supplier` VALUES ('3', '江苏亨通光电', '亨通光电', '', '', '18506292125', '30024167@qq.com', '江苏省', '苏州市', '吴江区', '七都镇', '', '', '', '招商银行', '', '', '0', '1553059145');

-- ----------------------------
-- Table structure for b_unit
-- ----------------------------
DROP TABLE IF EXISTS `b_unit`;
CREATE TABLE `b_unit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '单位名称',
  `en_name` varchar(20) DEFAULT NULL COMMENT '英文名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态: 0正常 1禁用',
  `create_time` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='计量单位';

-- ----------------------------
-- Records of b_unit
-- ----------------------------
INSERT INTO `b_unit` VALUES ('4', '台', null, '0', '1553059333');
INSERT INTO `b_unit` VALUES ('5', '个', null, '0', '1553059345');
