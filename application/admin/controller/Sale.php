<?php
namespace app\admin\controller;

class Sale extends Base
{

    public function getList(){
        return $this->object->getList();
    }

    public function prints($id){
        $this->assign( $this->object->prints($id) ); 
        return view();
    }

}
