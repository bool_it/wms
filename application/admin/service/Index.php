<?php
namespace app\admin\service;
use app\common\service\Base,
	app\admin\service\Chart,
	app\admin\model\AdminNode as Node;

/**
 * Index 首页逻辑处理
 * 作者:bool
 * QQ  :30024167
 */
class Index extends Base
{
	/**
	 * [__construct 构造方法]
	 */
	function __construct()
	{
		parent::__construct();
		// $this->model 	= new Model();
	}

	/**
	 * [index index方法]
	 * @return [bool] [结果集]
	 */
	public function index()
	{
		$menu['parent'] = Node::where([
			['status','=',0],
			['pid','=',0],
		])->select();

		$menu['child'] = Node::where([
			['status','=',0],
			['pid','>',0],
			['action','=','index'],
		])->select();
		
		$this->assign([
			'menu'	=>	$menu
		]);
    	return view();
	}

	// main
	public function main()
	{
		$chart = new Chart();

		$this->assign([
			'count_data'	=>	$chart->count_data(),
			'operation'		=>	$chart->operation(),
			'map_data'		=>	$chart->map(),
			'channel_data'	=>	$chart->channel()
		]);
    	return view();
	}

}
