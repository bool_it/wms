<?php
namespace app\admin\service;
use think\Db,
	app\common\service\Base,
	app\admin\model\Indepot as In_depot,
	app\admin\model\Outdepot as Out_depot;

class Depot extends Base
{
	/**
	 * [__construct 构造方法]
	 */
	function __construct()
	{
		parent::__construct();
	}

	// 入库
	public function in_depot($data,$type='add'){
		
		//检测保存类型
		if( $type == 'add' ){
			return $this->in_add($data);
		}else{
			return $this->in_update($data);
		}
	}

	// 添加入库
	public function in_add($data){
		// 事务操作
		Db::transaction(function () use($data) {

			$res = $data;
			unset($res['data']);
			unset($res['date']);
			$model = new In_depot();
			$id = $model->insertGetId($res);

			// 组装插入的数据
			$temp = [];
			foreach ($data['data'] as $k => $v) {

				//检测pid是否存在
				$is_pid = isset($v['pid']);

				// 不存在id说明没有关联id 是手动添加 改为id
				$is_pid || $v['pid'] = $v['id'];
				$is_pid || $data['data'][$k]['pid'] = $v['id'];

				$temp[] = [
					'fid' 			=> $id,
					'pid' 			=>  $v['pid'],
					'sid' 			=> $v['id'],
					'num' 			=> $v['num'],
					'price' 		=> $v['price'],
					'brand'			=>	$v['brand']['id'],
					'color'			=>	$v['color']['id'],
					'unit'			=>	$v['unit']['id'],
					'depot'			=>	$v['depot'],
					'location'		=>	$v['location'],
					'count'			=>	$v['count'],
					'create_time'	=>	time()
				];
			}
			// 插入全部
			model('IndepotMain')->insertAll($temp);

			// 更新订单状态
			$is_pid && model('purchase')->save(
				[ 'state' => 1 ],
				[ 'id'=> $data['purchase'] ] 
			);	

			// 修改仓库库存
			$stock = new Stock();
			$stock->setNum($data['data']);

			return ['error'	=>	0,'msg'	=>	'添加成功' ];
		});

		return ['error'	=>	0,'msg'	=>	'添加成功' ];
	}

	// 更新入库
	public function in_update($data){
		// 事务操作
		Db::transaction(function () use($data) {
			$model = new In_depot();
			//保存全部
			$model->update($data);

			// 组装插入的数据
			$temp = $addTemp = [];
			foreach ($data['data'] as $k => $v) {
				// 检测是否有该产品
				if( isset($v['sid']) ){
					$temp[] = [
						'id'			=> $v['id'],	
						'sid' 			=> $v['sid'],
						'pid' 			=> $v['pid'],
						'num' 			=> $v['num'],
						'price' 		=> $v['price'],
						'brand'			=>	$v['brand']['id'],
						'color'			=>	$v['color']['id'],
						'unit'			=>	$v['unit']['id'],
						'depot'			=>	$v['depot'],
						'location'		=>	$v['location'],
						'count'			=>	$v['count'],
					];
				}else{
					$addTemp[] = [
						'fid'			=> $data['id'],
						'sid' 			=> $data['id'],
						'pid' 			=> $v['id'],
						'num' 			=> $v['num'],
						'price' 		=> $v['price'],
						'brand'			=>	$v['brand']['id'],
						'color'			=>	$v['color']['id'],
						'unit'			=>	$v['unit']['id'],
						'depot'			=>	$v['depot'],
						'location'		=>	$v['location'],
						'count'			=>	$v['count'],
						'create_time'	=>	time()
					];
				}

			}
			// 更新
			model('IndepotMain')->saveAll($temp);
			// 新增
			model('IndepotMain')->insertAll($addTemp);

			// 检测是否有删除的数据
			isset($data['dels']) || $data['dels'] =[];
			// 删除
			model('IndepotMain')->destroy($data['dels']);

			// 修改仓库库存
			$stock = new Stock();
			$stock->setUpdateNum($data['data'], db('IndepotMain','in') );

			return ['error'	=>	0,'msg'	=>	'修改成功' ];
		});

		return ['error'	=>	0,'msg'	=>	'修改成功'];
	}


	// 出库
	public function out_depot($data,$type='add'){

		//检测保存类型
		if( $type == 'add' ){
			return $this->out_add($data);
		}else{
			return $this->out_update($data);
		}
	}

	// 出库添加
	public function out_add($data){
		// 事务操作
		Db::transaction(function () use($data) {

			$res = $data;
			unset($res['data']);
			unset($res['date']);
			$model = new Out_depot();
			$id = $model->insertGetId($res);

			// 组装插入的数据
			$temp = [];
			foreach ($data['data'] as $k => $v) {
				//检测pid是否存在
				$is_pid = isset($v['pid']);

				// 不存在id说明没有关联id 是手动添加 改为id
				$is_pid || $v['pid'] = $v['id'];
				$is_pid || $data['data'][$k]['pid'] = $v['id'];

				$temp[] = [
					'fid' 			=> $id,
					'pid' 			=>  $v['pid'],
					'sid' 			=> $v['id'],
					'num' 			=> $v['num'],
					'price' 		=> $v['price'],
					'brand'			=>	$v['brand']['id'],
					'color'			=>	$v['color']['id'],
					'unit'			=>	$v['unit']['id'],
					'depot'			=>	$v['depot'],
					'location'		=>	$v['location'],
					'count'			=>	$v['count'],
					'create_time'	=>	time()
				];
			}
			// 插入全部
			model('OutdepotMain')->insertAll($temp);

			// 更新订单状态
			$is_pid && model('sale')->save(
				[ 'state' => 1 ],
				[ 'id'=> $data['purchase'] ] 
			);	

			// 修改仓库库存
			$stock = new Stock();
			$stock->setNum($data['data'],'out');

			return ['error'	=>	0,'msg'	=>	'添加成功' ];
		});

		return ['error'	=>	0,'msg'	=>	'添加成功' ];
	}

	// 出库更新
	public function out_update($data){
		// 事务操作
		Db::transaction(function () use($data) {

			$model = new Out_depot();
			//保存全部
			$model->update($data);

			// 组装插入的数据
			$temp = $addTemp = [];
			foreach ($data['data'] as $k => $v) {
				// 检测是否有该产品
				if( isset($v['sid']) ){
					$temp[] = [
						'id'			=> $v['id'],	
						'sid' 			=> $v['sid'],
						'pid' 			=> $v['pid'],
						'num' 			=> $v['num'],
						'price' 		=> $v['price'],
						'brand'			=>	$v['brand']['id'],
						'color'			=>	$v['color']['id'],
						'unit'			=>	$v['unit']['id'],
						'depot'			=>	$v['depot'],
						'location'		=>	$v['location'],
						'count'			=>	$v['count'],
					];
				}else{
					$addTemp[] = [
						'fid'			=> $data['id'],
						'sid' 			=> $data['id'],
						'pid' 			=> $v['id'],
						'num' 			=> $v['num'],
						'price' 		=> $v['price'],
						'brand'			=>	$v['brand']['id'],
						'color'			=>	$v['color']['id'],
						'unit'			=>	$v['unit']['id'],
						'depot'			=>	$v['depot'],
						'location'		=>	$v['location'],
						'count'			=>	$v['count'],
						'create_time'	=>	time()
					];
				}

			}
			// 更新
			model('OutdepotMain')->saveAll($temp);
			// 新增
			model('OutdepotMain')->insertAll($addTemp);

			// 检测是否有删除的数据
			isset($data['dels']) || $data['dels'] =[];
			// 删除
			model('OutdepotMain')->destroy($data['dels']);

			// 修改仓库库存
			$stock = new Stock();
			$stock->setUpdateNum($data['data'], db('OutdepotMain'),'out' );

			return ['error'	=>	0,'msg'	=>	'修改成功' ];
		});

		return ['error'	=>	0,'msg'	=>	'修改成功'];
	}


}
