<?php

namespace app\admin\model;

use think\Model;

class Sale extends Model
{
    public function lists()
    {
        return $this->hasMany('SaleMain','sid')->selfRelation();
    }

    // 查询最近12个月销售情况
    public function getByMonth(){
        return $this->field("COUNT(id) as total,substring_index(sale_date, '-', 2) AS m")
                    ->group('m')
                    ->limit(0,12)
                    ->select();
    }

    // 获取各个月份的数据
    public function getMonth($date){

        $res = $this->field("COUNT(id) as total,SUM(count) as count,substring_index(sale_date, '-', 2) AS month")
                    ->where('sale_date','like','%'.$date.'%')
                    ->find();
        if(is_null($res['month'])){
            $res['month'] = $date;
            $res['count'] = 0;
        }
        return $res;
    }


}
