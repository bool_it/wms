<?php

namespace app\admin\model;

use think\Model;

class Purchase extends Model
{
    public function lists()
    {
        return $this->hasMany('PurchaseMain','sid')->selfRelation();
    }

    // 查询最近12个月采购情况
    public function getByMonth(){
        // SELECT COUNT(id) as total,substring_index(purchase_date, '-', 2) AS m FROM b_purchase GROUP BY m LIMIT 0,12
        return $this->field("COUNT(id) as total,substring_index(purchase_date, '-', 2) AS m")
                    ->group('m')
                    ->limit(0,12)
                    ->select();
    }

    // 获取各个月份的数据
    public function getMonth($date){

        $res = $this->field("COUNT(id) as total,SUM(count) as count,substring_index(purchase_date, '-', 2) AS month")
                    ->where('purchase_date','like','%'.$date.'%')
                    ->find();
        if(is_null($res['month'])){
            $res['month'] = $date;
            $res['count'] = 0;
        }
        return $res;
    }

    // 获取各个月份的数据
    // public function getMonth($date){
    //     // dump($date);
    //     return $this->field("COUNT(id) as total,SUM(count) as count,substring_index(purchase_date, '-', 2) AS month")
    //                 ->where('purchase_date','like','%'.$date.'%')
    //                 ->find();
    //     // dump( $this->getLastSql() );
    //     // return $res;
    // }


}
