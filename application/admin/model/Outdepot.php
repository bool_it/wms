<?php

namespace app\admin\model;

use think\Model;

class Outdepot extends Model
{

    // 获取供应商
    public function supplier()
    {
        return $this->hasOne('Supplier','id','supplier')->selfRelation();
    }

    public function customer()
    {
        return $this->hasOne('Customer','id','customer')->selfRelation();
    }    

    public function lists()
    {
        return $this->hasMany('IndepotMain','sid')->selfRelation();
    }    

    // 查询最近12个月出库情况
    public function getByMonth(){
        return $this->field("COUNT(id) as total,substring_index(in_date, '-', 2) AS m")
                    ->group('m')
                    ->limit(0,12)
                    ->select();
    }

    // 获取各个月份的数据
    public function getMonth($date){

        $res = $this->field("COUNT(id) as total,SUM(count) as count,substring_index(in_date, '-', 2) AS month")
                    ->where('in_date','like','%'.$date.'%')
                    ->find();
        if(is_null($res['month'])){
            $res['month'] = $date;
            $res['count'] = 0;
        }
        return $res;
    }


}
