![](https://images.gitee.com/uploads/images/2019/1225/193410_cb913fe8_1126226.png) 

基于 ThinkPHP 5.1（LTS版本） 开发
===============

# wms 进销存系统


### 测试账号

类型 | 账号 | 密码
---|---|---
管理员 |admin | admin


### 演示地址
http://148.70.120.105:8003/login/index.html

[演示地址](http://148.70.120.105:8003/login/index.html) 



## 桌面版下载  (.exe)
==注: 暂时只提供windows版 下载==

网盘 | 地址 | 密码 
---|---|---
90网盘 | https://www.90pan.com/b1586382 | o4o1
城通网盘 | https://t00y.com/file/23130714-415442905 | 396180
77file | https://t00y.com/file/23130714-415442905 | 396180


### 安装/更新框架核心 vendor （==可以跳过==）

	composer update



## qq群：785794314

<div  align="center">    
  <img src="./demo/alipay.jpg" width = "400" alt="图片名称" align=center />
</div>


---


## 演示
![1](./demo/1.gif)

![1](./demo/1.png)
![1](./demo/2.png)
![1](./demo/3.png)
![1](./demo/4.png)
![1](./demo/5.png)