// https://seajs.github.io/seajs/docs/#docs
seajs.config({
    // 变量配置
    vars: {
        'locale': 'zh-cn'
    },
    // 调试模式
    debug: true,
    // 文件编码
    charset: 'utf-8',
    alias: {
        'vue': 'vue.js',
        'highcharts': 'vendor/highcharts/highcharts.js',
        'highmaps': 'vendor/highcharts/highmaps.js',
        'china': 'vendor/highcharts/china.js'
    }
});
